import mosaik
import pytest
import pandas as pd
from os.path import dirname, join

PV_DATA = join(dirname(__file__), 'data', 'pv_10kw_test.csv')
PV_DATA2 = join(dirname(__file__), 'data', 'pv_10kw_test_2.csv')

SIM_CONFIG = {
    'CSV': {
        'python': 'mosaik_csv:CSV',
    },
    'CSVWriter': {
        'python': 'mosaik_csv_writer:CSVWriter'
    },
}
START = '2014-01-01 00:00:00'
END = 70 * 60


def run_scenario(sim_type):
    world = mosaik.World(SIM_CONFIG)

    # Starting simulators
    pvsim = world.start('CSV', sim_start=START, datafile=PV_DATA, type=sim_type)
    db = world.start('CSVWriter', start_date=START)

    # Creating instances
    pv = pvsim.PV()
    csv_writer = db.CSVWriter()

    # Connecting entities
    if sim_type == 'event-based':
        world.set_initial_event(pv.sid)  # Creating an initial event for the CSV simulator

    world.connect(pv, csv_writer, 'P')

    # Run simulation
    world.run(until=END)


def run_scenario2():
    world = mosaik.World(SIM_CONFIG)

    # Starting simulators
    pvsim = world.start('CSV', sim_start=START, datafile=PV_DATA, type='event-based')
    pvsim2 = world.start('CSV', sim_start=START, datafile=PV_DATA2, type='event-based')
    db = world.start('CSVWriter', start_date=START, output_file='results_2.csv')

    # Creating instances
    pv = pvsim.PV()
    pv2 = pvsim2.PV()
    #monitor = db.CSVWriter(attrs=['CSV-0.PV_0-P', 'CSV-1.PV_0-V'])
    monitor = db.CSVWriter()

    # Connecting entities
    world.set_initial_event(pv.sid)
    world.set_initial_event(pv2.sid)  # Creating an initial event for the CSV simulator

    world.connect(pv2, monitor, 'V')
    world.connect(pv, monitor, 'P')

    # Run simulation
    world.run(until=END)


@pytest.mark.parametrize('sim_type', [
    'time-based',
    'event-based',
])
def test_scenario_types(sim_type):
    run_scenario(sim_type)
    df_outfile = pd.read_csv('results.csv')
    df_expected = pd.read_csv(join(dirname(__file__), 'data', 'expected.csv'))

    match = df_outfile == df_expected #comparing files
    match = list(match.all()) #getting the result of the comparision

    assert match[0] and match[1], "The values have not been read correctly by mosaik-csv"


if __name__ == '__main__':
    #run_scenario('time-based')
    run_scenario2()
